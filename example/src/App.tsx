import React from "react";
import { Flag, useFlag } from "./flags";
import logo from "./logo.svg";
import "./App.css";

const FeatureComponent = ({ children = "Learn React" }) => (
  <a
    className="App-link"
    href="https://reactjs.org"
    target="_blank"
    rel="noopener noreferrer"
  >
    {children}
  </a>
);

const FeatureA = ({ children = "Feature A" }: any) => (
  <FeatureComponent>{children}</FeatureComponent>
);
const FeatureB = ({ children = "Feature B" }: any) => (
  <FeatureComponent>{children}</FeatureComponent>
);

const App: React.FC = () => {
  const lightBackground = useFlag("lightBackground");
  const smallLogo = useFlag("smallLogo");
  const bigFonts = useFlag("bigFonts");
  const style = lightBackground
    ? {
        background: "#5e5e5e",
        color: "black"
      }
    : {};

  return (
    <div className="App">
      <header className="App-header" style={style}>
        <img
          src={logo}
          className="App-logo"
          alt="logo"
          style={{
            zoom: smallLogo ? 0.5 : 1
          }}
        />
        <p
          style={{
            ...(bigFonts && {
              fontSize: "48px"
            })
          }}
        >
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <Flag
          name={"a"}
          render={() => <FeatureA>Feature A is active</FeatureA>}
        />
        <Flag
          name={"a"}
          fallbackRender={() => <FeatureA>Feature A is not active</FeatureA>}
        />
        <Flag name={"b"} render={() => <FeatureB />} />
        <Flag
          name={"nested"}
          render={flags => <div>Nested foo: {flags.nested.foo}</div>}
        />
      </header>
    </div>
  );
};

export default App;
