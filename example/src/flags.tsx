import { zeroConfigCreateFlags } from "feature-flags";

const defaultFlags = {
  lightBackground: false,
  smallLogo: false,
  bigFonts: true,
  a: true,
  b: 5,
  nested: {
    foo: 54
  }
};

const {
  Dashboard,
  Flag,
  FlagsProvider,
  PersistedDashboard,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
} = zeroConfigCreateFlags<typeof defaultFlags>()(defaultFlags);

export {
  Dashboard,
  Flag,
  FlagsProvider,
  PersistedDashboard,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
};
