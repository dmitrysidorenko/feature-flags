[feature-flags](../README.md) › [Globals](../globals.md) › ["utils/zeroConfigCreateFlags"](_utils_zeroconfigcreateflags_.md)

# External module: "utils/zeroConfigCreateFlags"

## Index

### Modules

* [__global](_utils_zeroconfigcreateflags_.__global.md)

### Object literals

* [defaultOptions](_utils_zeroconfigcreateflags_.md#const-defaultoptions)

## Object literals

### `Const` defaultOptions

### ▪ **defaultOptions**: *object*

Defined in utils/zeroConfigCreateFlags.tsx:11

###  globalName

• **globalName**: *string* = "FF_CONFIG"

Defined in utils/zeroConfigCreateFlags.tsx:13

###  processEnvName

• **processEnvName**: *string* = "FEATURE_FLAGS_CONFIG"

Defined in utils/zeroConfigCreateFlags.tsx:14

###  storageKey

• **storageKey**: *string* = "flags:persisted"

Defined in utils/zeroConfigCreateFlags.tsx:12
