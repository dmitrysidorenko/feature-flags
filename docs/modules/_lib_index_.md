[feature-flags](../README.md) › [Globals](../globals.md) › ["lib/index"](_lib_index_.md)

# External module: "lib/index"

## Index

### Functions

* [makeFeaturesApi](_lib_index_.md#makefeaturesapi)

## Functions

###  makeFeaturesApi

▸ **makeFeaturesApi**<**T**>(`config`: T): *[FeaturesApi](_types_feature_flags_.md#featuresapi)‹T›*

Defined in lib/index.ts:16

Example of usage

```typescript
const config = {
  feature1: true,
  feature2: false
};
const api = new makeFeaturesApi<typeof config>(config);
const feature1 = api.getFlag("feature1");
const feature2 = api.getFlag("feature2");
```

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

Name | Type |
------ | ------ |
`config` | T |

**Returns:** *[FeaturesApi](_types_feature_flags_.md#featuresapi)‹T›*
