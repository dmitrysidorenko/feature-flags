[feature-flags](../README.md) › [Globals](../globals.md) › ["types/feature-flags"](_types_feature_flags_.md)

# External module: "types/feature-flags"

## Index

### Type aliases

* [FeaturesApi](_types_feature_flags_.md#featuresapi)
* [FlagValue](_types_feature_flags_.md#flagvalue)
* [Flags](_types_feature_flags_.md#flags)
* [FlagsConfig](_types_feature_flags_.md#flagsconfig)
* [Keys](_types_feature_flags_.md#keys)
* [TableContext](_types_feature_flags_.md#tablecontext)
* [TableFlag](_types_feature_flags_.md#tableflag)

## Type aliases

###  FeaturesApi

Ƭ **FeaturesApi**: *object*

Defined in types/feature-flags.ts:5

#### Type declaration:

* **getFlag**(): *function*

  * <**K**>(`key`: K): *T[K]*

* **getFlags**(): *function*

  * (): *T*

* **setFlag**(): *function*

  * <**K**>(`key`: K, `value`: T[K]): *void*

* **setFlags**(): *function*

  * (`flags`: T): *void*

* **subscribe**(`cb`: function): *void*

___

###  FlagValue

Ƭ **FlagValue**: *string | number | boolean*

Defined in types/feature-flags.ts:2

___

###  Flags

Ƭ **Flags**: *object*

Defined in types/feature-flags.ts:3

#### Type declaration:

* \[ **key**: *string*\]: [FlagValue](_types_feature_flags_.md#flagvalue) | [Flags](_types_feature_flags_.md#flags)

___

###  FlagsConfig

Ƭ **FlagsConfig**: *object*

Defined in types/feature-flags.ts:14

#### Type declaration:

* **flags**: *T*

* **version**: *string*

___

###  Keys

Ƭ **Keys**: *keyof T*

Defined in types/feature-flags.ts:4

___

###  TableContext

Ƭ **TableContext**: *object*

Defined in types/feature-flags.ts:24

#### Type declaration:

* **features**: *[TableFlag](_types_feature_flags_.md#tableflag)‹T›[]*

___

###  TableFlag

Ƭ **TableFlag**: *object*

Defined in types/feature-flags.ts:19

#### Type declaration:

* **name**: *[Keys](_types_feature_flags_.md#keys)‹T›*

* **value**: *T[keyof T]*
