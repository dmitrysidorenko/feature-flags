[feature-flags](../README.md) › [Globals](../globals.md) › ["components/Table/Table"](_components_table_table_.md)

# External module: "components/Table/Table"

## Index

### Type aliases

* [SwitchProps](_components_table_table_.md#switchprops)
* [TableProps](_components_table_table_.md#tableprops)

### Functions

* [NumberInput](_components_table_table_.md#const-numberinput)
* [Switch](_components_table_table_.md#switch)
* [Table](_components_table_table_.md#table)
* [TextInput](_components_table_table_.md#const-textinput)

## Type aliases

###  SwitchProps

Ƭ **SwitchProps**: *object*

Defined in components/Table/Table.tsx:5

#### Type declaration:

* **checked**: *boolean*

* **onChange**(): *function*

  * (`value`: boolean): *void*

___

###  TableProps

Ƭ **TableProps**: *object*

Defined in components/Table/Table.tsx:19

#### Type declaration:

* **flags**: *[TableFlag](_types_feature_flags_.md#tableflag)‹T›[]*

* **onChange**(): *function*

  * (`change`: [TableFlag](_types_feature_flags_.md#tableflag)‹T›): *void*

* **showHeader**? : *undefined | false | true*

## Functions

### `Const` NumberInput

▸ **NumberInput**(`props`: object): *Element*

Defined in components/Table/Table.tsx:42

**Parameters:**

▪ **props**: *object*

Name | Type |
------ | ------ |
`onChange` | function |
`value` | number |

**Returns:** *Element*

___

###  Switch

▸ **Switch**(`props`: [SwitchProps](_components_table_table_.md#switchprops)): *ReactElement*

Defined in components/Table/Table.tsx:9

**Parameters:**

Name | Type |
------ | ------ |
`props` | [SwitchProps](_components_table_table_.md#switchprops) |

**Returns:** *ReactElement*

___

###  Table

▸ **Table**(`props`: [TableProps](_components_table_table_.md#tableprops)‹[Flags](_types_feature_flags_.md#flags)›): *ReactElement*

Defined in components/Table/Table.tsx:65

**Parameters:**

Name | Type |
------ | ------ |
`props` | [TableProps](_components_table_table_.md#tableprops)‹[Flags](_types_feature_flags_.md#flags)› |

**Returns:** *ReactElement*

___

### `Const` TextInput

▸ **TextInput**(`props`: object): *Element*

Defined in components/Table/Table.tsx:25

**Parameters:**

▪ **props**: *object*

Name | Type |
------ | ------ |
`onChange` | function |
`value` | string |

**Returns:** *Element*
