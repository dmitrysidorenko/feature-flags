[feature-flags](../README.md) › [Globals](../globals.md) › ["utils/index"](_utils_index_.md)

# External module: "utils/index"

## Index

### Modules

* [__global](_utils_index_.__global.md)

### Type aliases

* [AsyncConfigProvider](_utils_index_.md#asyncconfigprovider)
* [ConfigOptions](_utils_index_.md#configoptions)
* [ConfigSource](_utils_index_.md#configsource)
* [FuncConfigProvider](_utils_index_.md#funcconfigprovider)
* [SyncConfigSource](_utils_index_.md#syncconfigsource)

### Variables

* [DEFAULT_LOCAL_STORAGE_KEY](_utils_index_.md#const-default_local_storage_key)

### Functions

* [compareFlags](_utils_index_.md#const-compareflags)
* [fromEnvVariable](_utils_index_.md#const-fromenvvariable)
* [getConfig](_utils_index_.md#const-getconfig)
* [getConfigSync](_utils_index_.md#const-getconfigsync)
* [readFromJsonString](_utils_index_.md#const-readfromjsonstring)
* [readFromLocalStorage](_utils_index_.md#const-readfromlocalstorage)
* [writeToLocalStorage](_utils_index_.md#const-writetolocalstorage)

## Type aliases

###  AsyncConfigProvider

Ƭ **AsyncConfigProvider**: *function*

Defined in utils/index.ts:83

#### Type declaration:

▸ (): *Promise‹T›*

___

###  ConfigOptions

Ƭ **ConfigOptions**: *object*

Defined in utils/index.ts:78

#### Type declaration:

* **localStorageKey**? : *undefined | string*

* **variable**? : *[T](undefined)*

___

###  ConfigSource

Ƭ **ConfigSource**: *T | null | Promise‹T | null› | [AsyncConfigProvider](_utils_index_.md#asyncconfigprovider)‹T | null› | [FuncConfigProvider](_utils_index_.md#funcconfigprovider)‹T | null›*

Defined in utils/index.ts:85

___

###  FuncConfigProvider

Ƭ **FuncConfigProvider**: *function*

Defined in utils/index.ts:84

#### Type declaration:

▸ (): *T*

___

###  SyncConfigSource

Ƭ **SyncConfigSource**: *T | function | null*

Defined in utils/index.ts:115

## Variables

### `Const` DEFAULT_LOCAL_STORAGE_KEY

• **DEFAULT_LOCAL_STORAGE_KEY**: *"flags:persisted"* = "flags:persisted"

Defined in utils/index.ts:3

## Functions

### `Const` compareFlags

▸ **compareFlags**<**T**>(`defaultConfig`: T, `source`: [Flags](_types_feature_flags_.md#flags)): *boolean*

Defined in utils/index.ts:15

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

Name | Type |
------ | ------ |
`defaultConfig` | T |
`source` | [Flags](_types_feature_flags_.md#flags) |

**Returns:** *boolean*

___

### `Const` fromEnvVariable

▸ **fromEnvVariable**<**T**>(`name`: string): *(Anonymous function)*

Defined in utils/index.ts:68

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

Name | Type | Default |
------ | ------ | ------ |
`name` | string | "REACT_APP_FF_CONFIG" |

**Returns:** *(Anonymous function)*

___

### `Const` getConfig

▸ **getConfig**<**T**>(`def`: T, `validate`: function): *(Anonymous function)*

Defined in utils/index.ts:92

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

▪ **def**: *T*

▪`Default value`  **validate**: *function*=  compareFlags

▸ <**T**>(`defaultConfig`: T, `config`: any): *boolean*

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

Name | Type |
------ | ------ |
`defaultConfig` | T |
`config` | any |

**Returns:** *(Anonymous function)*

___

### `Const` getConfigSync

▸ **getConfigSync**<**T**>(`def`: T, `validate`: function): *(Anonymous function)*

Defined in utils/index.ts:116

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

▪ **def**: *T*

▪`Default value`  **validate**: *function*=  compareFlags

▸ (`defaultConfig`: T, `config`: T): *boolean*

**Parameters:**

Name | Type |
------ | ------ |
`defaultConfig` | T |
`config` | T |

**Returns:** *(Anonymous function)*

___

### `Const` readFromJsonString

▸ **readFromJsonString**<**T**>(`jsonString`: string): *T | null*

Defined in utils/index.ts:5

**Type parameters:**

▪ **T**: *__type*

**Parameters:**

Name | Type |
------ | ------ |
`jsonString` | string |

**Returns:** *T | null*

___

### `Const` readFromLocalStorage

▸ **readFromLocalStorage**<**T**>(`key`: string): *T | null*

Defined in utils/index.ts:46

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

Name | Type | Default |
------ | ------ | ------ |
`key` | string |  DEFAULT_LOCAL_STORAGE_KEY |

**Returns:** *T | null*

___

### `Const` writeToLocalStorage

▸ **writeToLocalStorage**(`flags`: object, `key`: string): *void*

Defined in utils/index.ts:53

**Parameters:**

Name | Type | Default |
------ | ------ | ------ |
`flags` | object | - |
`key` | string |  DEFAULT_LOCAL_STORAGE_KEY |

**Returns:** *void*
