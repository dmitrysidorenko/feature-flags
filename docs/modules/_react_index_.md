[feature-flags](../README.md) › [Globals](../globals.md) › ["react/index"](_react_index_.md)

# External module: "react/index"

## Index

### Type aliases

* [DashboardProps](_react_index_.md#dashboardprops)
* [OverlayProps](_react_index_.md#overlayprops)

### Functions

* [Overlay](_react_index_.md#const-overlay)
* [createFlags](_react_index_.md#const-createflags)

## Type aliases

###  DashboardProps

Ƭ **DashboardProps**: *object*

Defined in react/index.tsx:18

#### Type declaration:

* **onClose**(): *function*

  * (): *void*

* **overlay**: *boolean*

* **visible**: *boolean*

___

###  OverlayProps

Ƭ **OverlayProps**: *object*

Defined in react/index.tsx:15

#### Type declaration:

* **children**: *ReactChildren | ReactChild*

## Functions

### `Const` Overlay

▸ **Overlay**(`props`: [OverlayProps](_react_index_.md#overlayprops)): *ReactElement*

Defined in react/index.tsx:23

**Parameters:**

Name | Type |
------ | ------ |
`props` | [OverlayProps](_react_index_.md#overlayprops) |

**Returns:** *ReactElement*

___

### `Const` createFlags

▸ **createFlags**<**T**>(`defaultFlags`: T): *object*

Defined in react/index.tsx:63

Example:

```typescript
const config = {
  feature1: true,
  feature2: false
};
const {
  useFlagsTableContext,
  FlagsProvider,
  Dashboard,
  Flag,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
} = createFlags<typeof config>(config);
```

**Type parameters:**

▪ **T**: *[Flags](_types_feature_flags_.md#flags)*

**Parameters:**

Name | Type |
------ | ------ |
`defaultFlags` | T |

**Returns:** *object*

* **Dashboard**: *Dashboard*

* **Flag**: *Flag*

* **Flags**: *Flags*

* **FlagsProvider**: *FlagsProvider*

* **api**(): *object*

  * **getFlag**(): *function*

    * <**K**>(`key`: K): *T[K]*

  * **getFlags**(): *function*

    * (): *T*

  * **setFlag**(): *function*

    * <**K**>(`key`: K, `value`: T[K]): *void*

  * **setFlags**(): *function*

    * (`flags`: T): *void*

  * **subscribe**(`cb`: function): *void*

* **useFeaturesApi**: *useFeaturesApi*

* **useFlag**: *useFlag*

* **useFlags**: *useFlags*

* **useFlagsTableContext**: *useFlagsTableContext*

* **usePersistFlags**: *usePersistFlags*
