[feature-flags](../README.md) › [Globals](../globals.md) › ["utils/zeroConfigCreateFlags"](../modules/_utils_zeroconfigcreateflags_.md) › [__global](../modules/_utils_zeroconfigcreateflags_.__global.md) › [Window](_utils_zeroconfigcreateflags_.__global.window.md)

# Interface: Window

## Hierarchy

* **Window**

## Index

### Properties

* [FF_CONFIG](_utils_zeroconfigcreateflags_.__global.window.md#ff_config)
* [showFF](_utils_zeroconfigcreateflags_.__global.window.md#showff)

## Properties

###  FF_CONFIG

• **FF_CONFIG**: *[Flags](../modules/_types_feature_flags_.md#flags)*

Defined in utils/zeroConfigCreateFlags.tsx:19

___

###  showFF

• **showFF**: *function*

Defined in utils/zeroConfigCreateFlags.tsx:20

#### Type declaration:

▸ (`show`: boolean): *void*

**Parameters:**

Name | Type |
------ | ------ |
`show` | boolean |
