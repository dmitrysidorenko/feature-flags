[feature-flags](../README.md) › [Globals](../globals.md) › ["utils/index"](../modules/_utils_index_.md) › [__global](../modules/_utils_index_.__global.md) › [Window](_utils_index_.__global.window.md)

# Interface: Window

## Hierarchy

* **Window**

## Index

### Properties

* [FF_CONFIG](_utils_index_.__global.window.md#ff_config)

## Properties

###  FF_CONFIG

• **FF_CONFIG**: *[Flags](../modules/_types_feature_flags_.md#flags)*

Defined in utils/index.ts:64
