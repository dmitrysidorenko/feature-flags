[feature-flags](README.md) › [Globals](globals.md)

# feature-flags

# feature-flags

> A library for feature flagging. Provides React components and hooks

## Install

```bash
npm install --save feature-flags
```

## Usage

```tsx
// flags.tsx
import { zeroConfigCreateFlags } from "feature-flags";

const defaultFlags = {
  showHello: false,
  num: 5,
  str: "Hello",
  nested: {
    foo: 42
  }
};

const {
  Dashboard,
  Flag,
  FlagsProvider,
  PersistedDashboard,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
} = zeroConfigCreateFlags<typeof defaultFlags>()(defaultFlags);

export {
  Dashboard,
  Flag,
  FlagsProvider,
  PersistedDashboard,
  useFeaturesApi,
  useFlag,
  useFlags,
  usePersistFlags
};

// index.tsx
import React from "react";
import ReactDOM from "react-dom";
import { FlagsProvider, PersistedDashboard, useFlag } from "./flags";

const Root = () => {
  const showHello = useFlag("showHello")
  return (
    <FlagsProvider>
      <>
        {showHello && <h1>Hello!</h1>}
        <PersistedDashboard />
      </>
    </FlagsProvider>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));

```

## Documentation

Docs live [here](/docs/README.md)

## License

MIT © [](https://github.com/)
