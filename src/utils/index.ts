import { Flags } from "../types/feature-flags";

const DEFAULT_LOCAL_STORAGE_KEY = "flags:persisted";

export const readFromJsonString = <T extends {}>(
  jsonString: string
): T | null => {
  try {
    return JSON.parse(jsonString);
  } catch (error) {
    return null;
  }
};

export const compareFlags = <T extends Flags>(
  defaultConfig: T,
  source: Flags
): boolean => {
  const defaultKeys = Object.keys(defaultConfig).sort();
  const sourceKeys = Object.keys(source).sort();
  if (
    defaultKeys.length !== sourceKeys.length ||
    defaultKeys.join(",") !== sourceKeys.join(",")
  ) {
    return false;
  }
  const diff = defaultKeys.filter((key: string) => {
    const tVal = defaultConfig[key];
    const sVal = source[key];
    const tType = typeof tVal;
    const sType = typeof sVal;
    if (tType !== sType) {
      return true;
    }
    if (tType === "object") {
      if ((tVal && !sVal) || (sVal && !tVal)) {
        return true;
      }
      return !compareFlags<Flags>(tVal as Flags, sVal as Flags);
    }
    return false;
  });
  return diff.length === 0;
};

export const readFromLocalStorage = <T extends Flags>(
  key: string = DEFAULT_LOCAL_STORAGE_KEY
): T | null => {
  const str = localStorage.getItem(key) || "";
  return readFromJsonString<T>(str);
};

export const writeToLocalStorage = (
  flags: object,
  key: string = DEFAULT_LOCAL_STORAGE_KEY
) => {
  try {
    localStorage.setItem(key, JSON.stringify(flags));
  } catch (error) {}
};

declare global {
  interface Window {
    FF_CONFIG: Flags;
  }
}

export const fromEnvVariable = <T extends Flags>(
  name = "REACT_APP_FF_CONFIG"
) => (def: T) => {
  const value = process.env[name] && JSON.parse(process.env[name] || "");
  if (value && compareFlags<T>(value, def)) {
    return value;
  }
  return def;
};

export type ConfigOptions<T> = {
  variable?: T;
  localStorageKey?: string;
};

type AsyncConfigProvider<T> = () => Promise<T>;
type FuncConfigProvider<T> = () => T;
type ConfigSource<T extends Flags> =
  | T
  | null
  | Promise<T | null>
  | AsyncConfigProvider<T | null>
  | FuncConfigProvider<T | null>;

export const getConfig = <T extends Flags>(
  def: T,
  validate: <T extends Flags = {}>(
    defaultConfig: T,
    config: any
  ) => boolean = compareFlags
) => async (...sources: ConfigSource<T>[]): Promise<T> => {
  for (let source of sources) {
    if (typeof source === "function") {
      const conf = await source();
      if (conf && validate(def, conf)) {
        return conf;
      }
    }
    if (source) {
      const conf = (await source) as T;
      if (conf && validate(def, conf)) {
        return conf;
      }
    }
  }
  return def;
};
export type SyncConfigSource<T> = T | (() => T | null) | null;
export const getConfigSync = <T extends Flags>(
  def: T,
  validate: (defaultConfig: T, config: T) => boolean = compareFlags
) => (...sources: SyncConfigSource<T>[]): T => {
  for (let source of sources) {
    if (typeof source === "function") {
      const conf = source();
      if (conf && validate(def, conf)) {
        return conf;
      }
    }
  }
  return def;
};
