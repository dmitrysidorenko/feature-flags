import {
  getConfigSync,
  readFromLocalStorage,
  readFromJsonString,
  writeToLocalStorage
} from "./";
import { Flags } from "../types/feature-flags";
import { createFlags } from "../react";
import React, { useEffect } from "react";

const defaultOptions = {
  storageKey: "flags:persisted",
  globalName: "FF_CONFIG",
  processEnvName: "FEATURE_FLAGS_CONFIG"
};

declare global {
  interface Window {
    FF_CONFIG: Flags;
    showFF: (show: boolean) => void;
  }
}

export default <T extends Flags>(options?: typeof defaultOptions) => (
  defaultConfig: T
) => {
  const { storageKey, globalName, processEnvName } = {
    ...defaultOptions,
    ...options
  };

  const usePersistFlags = (key = storageKey) => {
    const flags = useFlags();
    useEffect(() => {
      writeToLocalStorage(flags, key);
    }, [flags, key]);
  };

  const config = getConfigSync<T>(defaultConfig)(
    () => readFromLocalStorage<T>(storageKey),
    window[globalName] as T,
    () =>
      readFromJsonString<T>(
        process &&
          process.env &&
          ((process.env[processEnvName] as string) || "")
      )
  );

  const {
    Flag,
    FlagsProvider,
    useFlag,
    useFlags,
    Dashboard,
    useFeaturesApi,
    api
  } = createFlags<T>(config);

  const useShowFromQueryString = (search = "", key = "ff:show") =>
    React.useMemo(() => {
      return new RegExp(`(^|^\\?:?|&)${key}($|&|=)`).test(search);
    }, [key, search]);

  const PersistedDashboard = (props: any) => {
    const queryParamsVisible = useShowFromQueryString(window.location.search);
    const [visible, setVisible] = React.useState(queryParamsVisible);
    React.useEffect(() => {
      window.showFF = (v = true) => setVisible(v);
    }, [setVisible]);

    usePersistFlags();

    return (
      <Dashboard
        overlay
        onClose={() => setVisible(false)}
        visible={visible}
        {...props}
      />
    );
  };
  return {
    api,
    Flag,
    FlagsProvider,
    useFlag,
    useFlags,
    PersistedDashboard,
    Dashboard,
    useFeaturesApi,
    usePersistFlags
  };
};
