import { compareFlags } from "./index";

describe("utils/compareFlags()", () => {
  it("returns true if arguments are the same", () => {
    // Arrange
    const def = {
      feature1: true,
      feature2: 42,
      nested: {
        foo: "bar"
      }
    };

    // Act
    const result = compareFlags(def, def);

    // Assert
    expect(result).toBe(true);
  });

  it("returns true if arguments are shallowly equal", () => {
    // Arrange
    const def = {
      feature1: true,
      feature2: 42,
      nested: {
        foo: "bar"
      }
    };

    // Act
    const result = compareFlags(def, { ...def });

    // Assert
    expect(result).toBe(true);
  });

  it("returns true if arguments are of the same shape", () => {
    // Arrange
    const def = {
      feature1: true,
      feature2: 42,
      nested: {
        foo: "bar"
      }
    };

    // Act
    const result = compareFlags(def, {
      feature1: false,
      feature2: 100500,
      nested: {
        foo: "42"
      }
    });

    // Assert
    expect(result).toBe(true);
  });

  it("returns false if a type of any value is of a different type", () => {
    // Arrange
    const def = {
      feature: true
    };

    // Act
    const result = compareFlags(def, {
      feature: 42
    });

    // Assert
    expect(result).toBe(false);
  });

  it("returns false if a type of any value is of a different type (a test for nested object)", () => {
    // Arrange
    const def = {
      feature: true,
      nested: {
        foo: "bar"
      }
    };

    // Act
    const result = compareFlags(def, {
      feature: true,
      nested: {
        foo: +42
      }
    });

    // Assert
    expect(result).toBe(false);
  });

  it("returns false if first argument misses a field", () => {
    // Arrange
    const def = {
      feature1: true
    };

    // Act
    const result = compareFlags(def, {
      feature1: true,
      feature2: true
    });

    // Assert
    expect(result).toBe(false);
  });

  it("returns true if second argument misses a field", () => {
    // Arrange
    const def = {
      feature1: true,
      feature2: true
    };

    // Act
    const result = compareFlags(def, {
      feature1: true
    });

    // Assert
    expect(result).toBe(false);
  });
});
