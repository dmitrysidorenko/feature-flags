import { makeFeaturesApi } from "./index";

describe("lib/makeFeaturesApi()", () => {
  let config = {
    feature1: true,
    feature2: true
  };

  it("creates api", () => {
    // Arrange
    const api = makeFeaturesApi(config);

    // Act

    // Assert
    expect(api).toBeTruthy();
  });

  it("api has getFlag method", () => {
    // Arrange
    const api = makeFeaturesApi(config);

    // Assert
    expect(api.getFlag).toBeInstanceOf(Function);
  });

  it("api has setFlag method", () => {
    // Arrange
    const api = makeFeaturesApi(config);

    // Assert
    expect(api.setFlag).toBeInstanceOf(Function);
  });

  it("api has getFlags method", () => {
    // Arrange
    const api = makeFeaturesApi(config);

    // Assert
    expect(api.getFlags).toBeInstanceOf(Function);
  });

  it("api has setFlags method", () => {
    // Arrange
    const api = makeFeaturesApi(config);

    // Assert
    expect(api.setFlags).toBeInstanceOf(Function);
  });

  it("getFlags() returns all flags", () => {
    // Arrange
    const config = {
      feature1: true,
      feature2: false
    };
    const api = makeFeaturesApi(config);

    // Act
    const flags = api.getFlags();

    // Assert
    expect(flags).toEqual(config);
  });

  it("setFlags() replaces all flags", () => {
    // Arrange
    const config = {
      feature1: true,
      feature2: false
    };
    const api = makeFeaturesApi(config);

    // Act
    const newConfig = { feature1: false, feature2: true };
    api.setFlags(newConfig);
    const flags = api.getFlags();

    // Assert
    expect(flags).toEqual(newConfig);
  });

  it("getFlag() returns correct value", () => {
    // Arrange
    const api = makeFeaturesApi({
      feature1: true
    });

    // Act
    const flag = api.getFlag("feature1");

    // Assert
    expect(flag).toBe(true);
  });

  it("setFlag() changes flag value", () => {
    // Arrange
    const api = makeFeaturesApi({
      feature1: true
    });

    // Act
    api.setFlag("feature1", false);
    const flag = api.getFlag("feature1");

    // Assert
    expect(flag).toBe(false);
  });

  it("setFlag() does not mutate initial config", () => {
    // Arrange
    const config = {
      feature1: true,
      nested: {
        feature2: true
      }
    };
    const api = makeFeaturesApi(config);

    // Act
    api.setFlag("nested", { feature2: false });
    const flags = api.getFlags();

    // Assert
    expect(flags === config).toBe(false);
    expect(flags.nested === config.nested).toBe(false);
  });

  it("calls subscribe callback when any setFlag() is called with new value", () => {
    // Arrange
    const config = {
      feature1: true
    };
    const api = makeFeaturesApi(config);
    const mockCallback = jest.fn((current, prev) => {});

    // Act
    api.subscribe(mockCallback);
    api.setFlag("feature1", false);

    // Assert
    expect(mockCallback.mock.calls.length).toBe(1);
  });

  it("does not call subscribe callback when any setFlag() is called with the same value", () => {
    // Arrange
    const config = {
      feature1: true
    };
    const api = makeFeaturesApi(config);
    const mockCallback = jest.fn((current, prev) => {});

    // Act
    api.subscribe(mockCallback);
    api.setFlag("feature1", true);

    // Assert
    expect(mockCallback.mock.calls.length).toBe(0);
  });

  it("calls subscribe callback when setFlags() is called with new config", () => {
    // Arrange
    const config = {
      feature1: true
    };
    const api = makeFeaturesApi(config);
    const mockCallback = jest.fn((current, prev) => {});

    // Act
    api.subscribe(mockCallback);
    api.setFlags({ feature1: false });

    // Assert
    expect(mockCallback.mock.calls.length).toBe(1);
  });

  it("calls subscribe callback when setFlags() is called with the same config", () => {
    // Arrange
    const config = {
      feature1: true
    };
    const api = makeFeaturesApi(config);
    const mockCallback = jest.fn(() => {});

    // Act
    api.subscribe(mockCallback);
    api.setFlags({ feature1: true });

    // Assert
    expect(mockCallback.mock.calls.length).toBe(1);
  });
});
