import { Flags, Keys, FeaturesApi } from "../types/feature-flags";

/**
 * Example of usage
 *
 * ```typescript
 * const config = {
 *   feature1: true,
 *   feature2: false
 * };
 * const api = new makeFeaturesApi<typeof config>(config);
 * const feature1 = api.getFlag("feature1");
 * const feature2 = api.getFlag("feature2");
 * ```
 */
export function makeFeaturesApi<T extends Flags>(config: T): FeaturesApi<T> {
  let mutableConfig = Object.assign({}, config);
  const callbacks: (() => void)[] = [];

  function getFlag<K extends Keys<T>>(key: K): T[K] {
    return mutableConfig[key];
  }

  function setFlag<K extends Keys<T>>(key: K, value: T[K]): void {
    const prev = mutableConfig;
    mutableConfig = Object.assign({}, mutableConfig, { [key]: value });
    if (prev[key] !== mutableConfig[key]) {
      callbacks.forEach(cb => cb());
    }
  }

  function getFlags() {
    return mutableConfig;
  }

  function setFlags(newFlags: T) {
    mutableConfig = Object.assign({}, newFlags);
    callbacks.forEach(cb => cb());
  }

  function subscribe(cb: () => void) {
    if (!callbacks.includes(cb)) {
      callbacks.push(cb);
    }
  }

  return {
    getFlag,
    setFlag,
    getFlags,
    setFlags,
    subscribe
  };
}
