import { Table, createFlags, Overlay, OverlayProps } from "./react";
export {
  FlagsConfig,
  TableContext,
  TableFlag,
  FeaturesApi,
  FlagValue,
  Flags,
  Keys
} from "./types/feature-flags";
export { makeFeaturesApi } from "./lib";
export {
  default as zeroConfigCreateFlags
} from "./utils/zeroConfigCreateFlags";
export {
  compareFlags,
  getConfig,
  getConfigSync,
  readFromLocalStorage,
  readFromJsonString,
  writeToLocalStorage
} from "./utils";

export { Table, createFlags, Overlay, OverlayProps };
export default createFlags;
