import React, {
  ReactElement,
  ReactNode,
  ReactChildren,
  ReactChild,
  useEffect
} from "react";
import { FeaturesApi, Flags } from "../types/feature-flags";
import { makeFeaturesApi } from "../lib";
import { writeToLocalStorage } from "../utils";
import Table from "../components/Table";
import { ReactComponentLike } from "prop-types";

export { Table };
export type OverlayProps = {
  children: ReactChildren | ReactChild;
};
export type DashboardProps = {
  overlay: boolean;
  visible: boolean;
  onClose: () => void;
};
export const Overlay = (props: OverlayProps): ReactElement => {
  const { children } = props;
  return (
    <div
      style={{
        display: "block",
        position: "fixed",
        top: 0,
        left: 0,
        width: "100vw",
        overflow: "auto",
        height: "100vh",
        backgroundColor: "rgba(0,0,0,0.5)"
      }}
    >
      {children}
    </div>
  );
};

/**
 * Example:
 *
 * ```typescript
 * const config = {
 *   feature1: true,
 *   feature2: false
 * };
 * const {
 *   useFlagsTableContext,
 *   FlagsProvider,
 *   Dashboard,
 *   Flag,
 *   useFeaturesApi,
 *   useFlag,
 *   useFlags,
 *   usePersistFlags
 * } = createFlags<typeof config>(config);
 * ```
 */
export const createFlags = <T extends Flags>(defaultFlags: T) => {
  const api = makeFeaturesApi(defaultFlags);
  const ApiContext = React.createContext<FeaturesApi<T>>(api);
  const FlagsContext = React.createContext<T>(defaultFlags);

  const useFlag = <K extends keyof T>(name: K) => {
    const ctx = React.useContext<T>(FlagsContext);
    return ctx[name];
  };
  const useFlags = () => {
    const ctx = React.useContext<T>(FlagsContext);
    return ctx;
  };
  const useFeaturesApi = () => {
    const ctx = React.useContext<FeaturesApi<T>>(ApiContext);
    return ctx;
  };

  type FlagsProviderProps = {
    children: ReactNode | null;
    value?: T;
  };

  const FlagsProvider = ({ children, value }: FlagsProviderProps) => {
    const [cache, updateCache] = React.useState<T>(value || defaultFlags);
    const apiProxy: FeaturesApi<T> = React.useMemo(() => {
      api.subscribe(() => {
        updateCache(api.getFlags());
      });
      return api;
    }, [api, updateCache]);
    return (
      <ApiContext.Provider value={apiProxy}>
        <FlagsContext.Provider value={cache}>{children}</FlagsContext.Provider>
      </ApiContext.Provider>
    );
  };

  type FlagProps<T, K extends keyof T> = {
    name: K;
    render?: (flags: T) => ReactElement | null;
    fallbackRender?: (flags: T) => ReactElement | null;
    component?: ReactComponentLike;
    fallbackComponent?: ReactComponentLike;
    test?(flag: T[K], flags: T): boolean;
  };
  const Flag = <K extends keyof T>({
    name,
    render,
    fallbackRender,
    component: Component,
    fallbackComponent: FallbackComponent,
    test = v => Boolean(v)
  }: FlagProps<T, K>): ReactElement<any> | null => {
    const flags = useFlags();
    const active = test(flags[name], flags);
    if (active) {
      return render ? (
        render(flags)
      ) : Component ? (
        <Component flags={flags} />
      ) : null;
    }
    return fallbackRender ? (
      fallbackRender(flags)
    ) : FallbackComponent ? (
      <FallbackComponent flags={flags} />
    ) : null;
  };

  type FlagsProps = {
    render?: (flags: T) => ReactElement | null;
    fallbackRender?: (flags: T) => ReactElement | null;
    component?: ReactComponentLike;
    fallbackComponent?: ReactComponentLike;
  };
  const Flags = ({
    render,
    component: Component
  }: FlagsProps): ReactElement | null => {
    const flags = useFlags();
    return render ? (
      render(flags)
    ) : Component ? (
      <Component flags={flags} />
    ) : null;
  };

  const useFlagsTableContext = () => {
    const api = useFeaturesApi();
    const flags = useFlags();
    return React.useMemo(() => {
      const flagKeys = Object.keys(flags);
      const tableFeatures = flagKeys.map((k: string) => {
        const v = flags[k];
        return {
          name: k,
          value: v
        };
      });

      return {
        features: tableFeatures,
        onChange: <K extends keyof T>({
          name,
          value
        }: {
          name: K;
          value: T[K];
        }) => {
          api.setFlag(name, value);
        }
      };
    }, [flags, api]);
  };

  const Dashboard = (props: DashboardProps): ReactElement | null => {
    const { overlay = false, visible = true, onClose = () => {} } = props;
    if (!visible) {
      return null;
    }

    const data = useFlagsTableContext();
    const api = useFeaturesApi();

    const table = (
      <Table
        flags={data.features}
        onChange={f => {
          const v = f.value as any;
          api.setFlag(f.name, v);
        }}
      />
    );
    if (overlay) {
      return (
        <Overlay>
          <>
            {table}
            <div style={{ textAlign: "right", padding: 5 }}>
              <button
                style={{
                  fontSize: "18px",
                  borderRadius: 4
                }}
                onClick={() => onClose()}
              >
                Close
              </button>
            </div>
          </>
        </Overlay>
      );
    }
    return table;
  };

  const usePersistFlags = (key: string) => {
    const flags = useFlags();
    useEffect(() => {
      writeToLocalStorage(flags, key);
    }, [flags, key]);
  };

  return {
    api,
    FlagsProvider,
    Flags,
    Flag,
    Dashboard,
    useFlagsTableContext,
    useFeaturesApi,
    useFlag,
    useFlags,
    usePersistFlags
  };
};
