// lib
export type FlagValue = string | number | boolean;
export type Flags = { [key: string]: FlagValue | Flags };
export type Keys<T> = keyof T;
export type FeaturesApi<T extends Flags> = {
  getFlag: <K extends Keys<T>>(key: K) => T[K];
  setFlag: <K extends Keys<T>>(key: K, value: T[K]) => void;
  setFlags: (flags: T) => void;
  getFlags: () => T;
  subscribe(cb: () => void): void;
};

// react
export type FlagsConfig<T extends Flags> = {
  flags: T;
  version: string;
};

export type TableFlag<T extends Flags> = {
  name: Keys<T>;
  value: T[Keys<T>];
};

export type TableContext<T extends Flags> = {
  features: TableFlag<T>[];
};
