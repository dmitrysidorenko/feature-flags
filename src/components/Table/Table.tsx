import React, { ReactElement } from "react";
import styles from "./Table.css";
import { TableFlag, Flags, FlagValue } from "../../types/feature-flags";

type SwitchProps = {
  checked: boolean;
  onChange: (value: boolean) => void;
};
function Switch(props: SwitchProps): ReactElement {
  return (
    <input
      type="checkbox"
      checked={props.checked}
      onChange={() => props.onChange(!props.checked)}
    />
  );
}

export type TableProps<T extends Flags> = {
  flags: TableFlag<T>[];
  onChange: (change: TableFlag<T>) => void;
  showHeader?: boolean;
};

const TextInput = (props: { value: string; onChange: (v: string) => void }) => {
  const [val, setVal] = React.useState<string>(props.value);
  React.useEffect(() => {
    setVal(props.value);
  }, [props.value]);
  return (
    <input
      type="text"
      value={val}
      onChange={e => {
        const v = e.target.value;
        setVal(v);
        props.onChange(v);
      }}
    />
  );
};
const NumberInput = (props: {
  value: number;
  onChange: (v: number) => void;
}) => {
  const { onChange, value, ...rest } = props;
  const [val, setVal] = React.useState<number>(value);
  React.useEffect(() => {
    setVal(value);
  }, [value]);
  return (
    <input
      type="number"
      value={val}
      onChange={e => {
        const v = Number(e.target.value);
        setVal(v);
        props.onChange(v);
      }}
      {...rest}
    />
  );
};

export default function Table(props: TableProps<Flags>): React.ReactElement {
  const { flags = [], onChange, showHeader = true } = props;

  return (
    <table className={styles.Table}>
      {showHeader && (
        <thead>
          <tr>
            <th>Feature</th>
            <th className={styles.Table__status_cell}>State</th>
          </tr>
        </thead>
      )}
      <tbody>
        {flags.map(flag => {
          let flagValueUi = null;
          switch (typeof flag.value) {
            case "boolean":
              const ch = flag.value as boolean;
              flagValueUi = (
                <Switch
                  checked={ch}
                  onChange={v => {
                    onChange({ ...flag, value: v });
                  }}
                />
              );
              break;
            case "string":
              const t = flag.value as string;
              flagValueUi = (
                <TextInput
                  value={t}
                  onChange={v => {
                    const value = v as FlagValue;
                    onChange({ name: flag.name, value });
                  }}
                />
              );
              break;
            case "number":
              const n = flag.value as number;
              flagValueUi = (
                <NumberInput
                  value={n}
                  onChange={v => {
                    const value = v as FlagValue;
                    onChange({ name: flag.name, value });
                  }}
                />
              );
              break;
            case "object": {
              const vv = flag.value as Flags;
              const nestedFlags = Object.keys(vv).map(k => {
                return {
                  name: k,
                  value: flag.value[k]
                };
              });

              flagValueUi = (
                <Table
                  showHeader={false}
                  flags={nestedFlags}
                  onChange={v => {
                    v.name;
                    const value = Object.assign(
                      {},
                      flag.value,
                      Object.assign({}, flag.value[v.name], {
                        [v.name]: v.value
                      })
                    );
                    onChange({ name: flag.name, value });
                  }}
                />
              );
              break;
            }
            default: {
            }
          }
          return (
            <tr key={flag.name}>
              <td>
                <div>{flag.name}</div>
              </td>
              <td className={styles.Table__status_cell}>{flagValueUi}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
